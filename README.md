# Term Structure - local DB environment setup

This is build with PostgresSQL 14

## environment needs

| NAME | Description |
|------|-------------|
|HOST_DIR| absolutely path for data store on host machine|
|POSTGRES_PORT| service port on host machine|
|POSTGRES_USER| db auth user name|
|POSTGRES_PASSWORD| db auth password |
|POSTGRES_DB| db default auth dbname |

## for outside access

need to edit 2 files under /var/lib/postgresql/data folder 

pg_hba.conf
postgresql.conf